import { get, writable } from 'svelte/store';
import Theme, { type ThemePalette, type ThemeText } from '../lib/theme';
import createMonochromeTheme from '../lib/theme-monochrome';
import createAccentTheme from '../lib/theme-accent';
import ThemeBuilder from '../lib/theme-builder';
import Color from '../lib/color';

export type Texts = { [K in keyof ThemeText]: string };

export const colorOrder = [
  'primary',
  'secondary',
  'tertiary',
  'success',
  'warning',
  'danger',
] as unknown as Array<keyof ThemePalette>;

export enum ThemeTypes {
  Monochrome,
  Accent,
  TwoAccents,
  ThreeAccents
}

const ThemeBuilders = {
  [ThemeTypes.Monochrome]: createMonochromeTheme,
  [ThemeTypes.Accent]: createAccentTheme,
  [ThemeTypes.TwoAccents]: createAccentTheme,
} as Record<ThemeTypes, (...args: any) => Theme>;

export const editing = writable(false);
export const themeMode = writable('scss');
export const themeAdvanced = writable(false);

export const themeType = writable<ThemeTypes>(ThemeTypes.Monochrome);
export const themeLight = writable(false);
export const theme = writable<Theme | null>(null);

export const palette = writable<Partial<ThemePalette>>({});
export const texts = writable<Texts>({
  text: '#dedede',
});

export const addColor = () => {
  palette.update(c => {
    const nextIndex = Object.keys(c).length;
    if (colorOrder.length > nextIndex) {
      const key = colorOrder[nextIndex] as keyof ThemePalette;
      c[key] = '#aaaaff';
    }

    return c;
  });
};

export const removeColor = () => {
  palette.update(c => {
    const currentIndex = Object.keys(c).length - 1;
    const key = colorOrder[currentIndex] as keyof ThemePalette;
    delete c[key];
    return c;
  });
};

export const useThemeWithColor = (color: string) => {
  const c = new Color(color);

  const newTheme = new ThemeBuilder()
    .setPrefix('preview')
    .addPalette('primary', {
      bg: c,
      fg: c.isDark ? new Color('#181818') : new Color('#dedede'),
    })
    .get();

  theme.set(newTheme);
};

export const useThemeWithColors = (colors: Partial<ThemePalette>) => {
  const entries = Object.entries(colors).map(([k, v]) => [k, new Color(v)]);
  const builder = new ThemeBuilder().setPrefix('preview');

  const dark = new Color('#181818');
  const light = new Color('#dedede');

  for (const [k, v] of entries as Array<[keyof ThemePalette, Color]>) {
    builder.addPalette(k, {
      bg: v,
      fg: v.isDark ? light : dark,
    });
  }

  theme.set(builder.get());
};

const updateTheme = (texts: Partial<Texts>, palette: Partial<ThemePalette>) => {
  const isDark = !get(themeLight);
  const type = get(themeType);
  const tEntries = Object.fromEntries(
    Object.entries(texts).map(([k, v]) => [k, new Color(v)]));
  const pEntries = Object.fromEntries(
    Object.entries(palette).map(([k, v]) => [k, new Color(v)]));

  if (type in ThemeBuilders) {
    const newTheme = ThemeBuilders[type](isDark, tEntries, pEntries);
    if (newTheme) theme.set(newTheme);
  }
};

// When text colors are updated, update the theme as well
texts.subscribe(t => {
  const p = get(palette);
  updateTheme(t, p);
});

// When colors are updated, update the theme as well
palette.subscribe(p => {
  const t = get(texts);
  updateTheme(t, p);
});

// When the theme type changes, update the number of palette colors
themeType.subscribe(type => {
  const num = type as number;
  palette.update(p => {
    return Object.fromEntries(
      colorOrder.slice(0, num).map(k => [k, p[k] || '#aaaaff']),
    );
  });
});

themeLight.subscribe(() => {
  const t = get(texts);
  const p = get(palette);
  updateTheme(t, p);
});
