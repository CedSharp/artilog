export const upperFirst = (text: string) =>
  text.length < 2
    ? text.toUpperCase()
    : text[0].toUpperCase() + text.substring(1);

export const pretty = (text: string) =>
  text.split('-').map(upperFirst).join(' ');
