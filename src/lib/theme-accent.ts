import Color from './color';
import Theme from './theme';
import ThemeBuilder from './theme-builder';

export default (isDark: boolean, texts: Record<string, Color>, palette: Record<string, Color>): Theme => {
  const ground = texts.text.withLuminosity(isDark ? 15 : 85);

  const dark =
    ground.isDark ? ground :
      texts.text.isDark ? texts.text :
        texts.text.withLuminosity(10);

  const light =
    ground.isLight ? ground :
      texts.text.isLight ? texts.text :
        texts.text.withLuminosity(90);

  const secondary = palette.secondary || texts.text.mixWith(ground, 0.25);
  const border = texts.text.mixWith(ground, 0.5);

  return new ThemeBuilder()
    .setPrefix('preview')
    .addText('text', texts.text)
    .addSurface('ground', { bg: ground, fg: texts.text })
    .addForm('border', border)
    .addPalette('primary', {
      bg: palette.primary,
      fg: palette.primary.isDark ? light : dark,
    })
    .addPalette('secondary', {
      bg: secondary,
      fg: secondary.isDark ? light : dark,
    })
    .get();
};
