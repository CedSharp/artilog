import Theme, { createPalette } from './theme';
import type {
  ThemeForm,
  ThemePaletteTokens,
  ThemeSurfaceTokens,
  ThemeText,
  ThemeToken,
} from './theme';
import Color from './color';

export default class ThemeBuilder {
  protected static readonly DEFAULT_TEXT = new Color('#dedede');
  protected static readonly DEFAULT_FOCUS = new Color('#7fA2fb');
  protected static readonly DEFAULT_PRIMARY = new Color('#aaaaff');
  protected static readonly DEFAULT_SECONDARY = new Color('#888888');
  protected static readonly DEFAULT_TERTIARY = new Color('#aaaaaa');
  protected static readonly DEFAULT_SUCCESS = new Color('#4bb543');
  protected static readonly DEFAULT_WARNING = new Color('#ffc107');
  protected static readonly DEFAULT_DANGER = new Color('#ff0000');

  protected prefix = '';
  protected texts: Partial<ThemeText> = {};
  protected forms: Partial<ThemeForm> = {};
  protected palette: Partial<ThemePaletteTokens> = {};
  protected surface: Partial<ThemeSurfaceTokens> = {};

  setPrefix(prefix: string) {
    this.prefix = prefix;
    return this;
  }

  addText(prop: keyof ThemeText, color: Color) {
    this.texts[prop] = color;
    return this;
  }

  addForm(prop: keyof ThemeForm, color: Color) {
    this.forms[prop] = color;
    return this;
  }

  addPalette(prop: keyof ThemePaletteTokens, token: ThemeToken) {
    this.palette[prop] = token;
    return this;
  }

  addSurface(prop: keyof ThemeSurfaceTokens, token: ThemeToken) {
    this.surface[prop] = token;
    return this;
  }

  get(): Theme {
    const texts = this.getTexts();
    const forms = this.getForms(texts);
    const surfaces = this.getSurfaces(texts);
    const palette = this.getPalette(texts, surfaces);

    return new Theme(this.prefix || 'theme', texts, forms, palette, surfaces);
  }

  protected getTexts(): ThemeText {
    return {
      text: this.texts.text || ThemeBuilder.DEFAULT_TEXT,
    };
  }

  protected getForms(texts: ThemeText): ThemeForm {
    const border = this.forms.border || texts.text;
    const hover = this.forms['hover-border'] || border.lighten(0.3);
    const focus = this.forms['focus-border'] || ThemeBuilder.DEFAULT_FOCUS;

    return {
      border,
      'hover-border': hover,
      'focus-border': focus,
    };
  }

  protected getSurfaces(texts: ThemeText): ThemeSurfaceTokens {
    const base = this.surface.ground?.bg || texts.text.withLuminosity(15);
    const dark = base.isDark ? base : texts.text;
    const light = base.isDark ? texts.text : base;

    const ground = this.surface.ground || {
      fg: light,
      bg: dark,
    };

    const content = this.surface.content || {
      fg: light,
      bg: dark.lighten(0.3),
    };

    return { ground, content };
  }

  protected getPalette(texts: ThemeText, surfaces: ThemeSurfaceTokens): ThemePaletteTokens {
    const dark = surfaces.ground.bg.isDark
      ? surfaces.ground.bg
      : surfaces.ground.fg;
    const light = surfaces.ground.bg.isDark
      ? surfaces.ground.fg
      : surfaces.ground.bg;

    const primary = this.palette.primary || {
      bg: ThemeBuilder.DEFAULT_PRIMARY,
      fg: ThemeBuilder.DEFAULT_PRIMARY.isDark ? light : dark,
    };

    const secondary = this.palette.secondary || {
      bg: ThemeBuilder.DEFAULT_SECONDARY,
      fg: ThemeBuilder.DEFAULT_SECONDARY.isDark ? light : dark,
    };

    const tertiary = this.palette.tertiary || {
      bg: ThemeBuilder.DEFAULT_TERTIARY,
      fg: ThemeBuilder.DEFAULT_TERTIARY.isDark ? light : dark,
    };

    const success = this.palette.success || {
      bg: ThemeBuilder.DEFAULT_SUCCESS,
      fg: ThemeBuilder.DEFAULT_SUCCESS.isDark ? light : dark,
    };

    const warning = this.palette.warning || {
      bg: ThemeBuilder.DEFAULT_WARNING,
      fg: ThemeBuilder.DEFAULT_WARNING.isDark ? light : dark,
    };

    const danger = this.palette.danger || {
      bg: ThemeBuilder.DEFAULT_DANGER,
      fg: ThemeBuilder.DEFAULT_DANGER.isDark ? light : dark,
    };

    return {
      primary,
      secondary,
      tertiary,
      success,
      warning,
      danger,
    };
  }
}
