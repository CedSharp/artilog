import Color from './color';

export interface ThemeText {
  text: Color;
}

export interface ThemeForm {
  border: Color;
  'hover-border': Color;
  'focus-border': Color;
}

export interface ThemeSurface {
  ground: string;
  content: string;
}

export interface ThemePalette {
  primary: string;
  secondary: string;
  tertiary: string;
  success: string;
  warning: string;
  danger: string;
}

export interface ThemeToken {
  fg: Color;
  bg: Color;
}

export type ThemeSurfaceTokens = {
  [K in keyof ThemeSurface]: ThemeToken
}

export type ThemePaletteTokens = {
  [K in keyof ThemePalette]: ThemeToken
}

const textToCss = (data: ThemeText | ThemeForm) => Object.fromEntries(
  Object.entries(data).map(
    ([k, v]) => [k, v.toString()],
  ),
) as Record<string, string>;

const tokenToCss = (data: ThemePaletteTokens | ThemeSurfaceTokens) =>
  Object.fromEntries(Object.entries(data).map(
    ([k, v]) => [k, { fg: v.fg.toString(), bg: v.bg.toString() }],
  ));

export const createPalette = (c: Color) => ({
  c100: c.darken(0.8),
  c200: c.darken(0.6),
  c300: c.darken(0.4),
  c400: c.darken(0.2),
  c500: c,
  c600: c.lighten(0.2),
  c700: c.lighten(0.4),
  c800: c.lighten(0.6),
  c900: c.lighten(0.8),
});

const cssToString = (prefix: string, css: Record<string, string | { bg: string; fg: string }>) => {
  return Object.entries(css).map(([k, v]) => {
    return typeof v === 'string'
      ? `--${prefix}-${k}: ${v}`
      : `--${prefix}-${k}: ${v.bg}; --${prefix}-on-${k}: ${v.fg}`;
  });
};

export default class Theme {
  constructor(
    public prefix: string,
    public texts: ThemeText,
    public forms: ThemeForm,
    public palette: ThemePaletteTokens,
    public surface: ThemeSurfaceTokens,
  ) {
  }

  toStyles() {
    return [
      textToCss(this.texts),
      textToCss(this.forms),
      tokenToCss(this.palette),
      tokenToCss(this.surface),
    ]
      .map(v => cssToString(this.prefix, v).join('; '))
      .join('; ');
  }
}

