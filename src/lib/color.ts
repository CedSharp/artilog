const RGBA = /rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d\.?\d*))?\)/;
const HSLA = /hsla?\((\d+)(?:deg)?,\s*(\d+)%,\s*(\d+)%(?:,\s*(\d\.?\d*))?\)/;

export default class Color {
  hue: number = 0;
  sat: number = 0;
  lum: number = 0;
  alp: number = 1;

  constructor(color: string)
  constructor(h: number, s: number, l: number, a: number)
  constructor() {
    if (typeof arguments[0] === 'string') {
      const color = arguments[0];
      if (color.startsWith('#')) this.setHex(color);
      else if (color.startsWith('hsl')) this.setHsl(color);
      else if (color.startsWith('rgb')) this.setRgb(color);
      else throw new Error('Invalid color format: ' + color);
    } else {
      this.hue = arguments[0];
      this.sat = arguments[1];
      this.lum = arguments[2];
      this.alp = arguments.length === 4 ? arguments[3] : 1;
    }
  }

  get isDark() {
    return this.lum < 60;
  }

  get isLight() {
    return this.lum >= 60;
  }

  setRgb(r: number, g: number, b: number, a: number): void;
  setRgb(r: number, g: number, b: number): void;
  setRgb(hex: string): void;
  setRgb() {
    if (typeof arguments[0] === 'string') {
      const match = arguments[0].match(RGBA);
      if (!match) throw new Error('Invalid hex format: ' + arguments[0]);
      const r = parseInt(match[1] || '0');
      const g = parseInt(match[2] || '0');
      const b = parseInt(match[3] || '0');
      const a = parseFloat(match[4] || '1');
      const [h, s, l] = Color.rgbToHsl(r, g, b);
      this.setHsl(h, s, l, a);
    } else {
      const r = arguments[0];
      const g = arguments[1];
      const b = arguments[2];
      const a = arguments.length === 4 ? arguments[3] : 1;
      const [h, s, l] = Color.rgbToHsl(r, g, b);
      this.setHsl(h, s, l, a);
    }
  }

  setHsl(h: number, s: number, l: number, a: number): void;
  setHsl(h: number, s: number, l: number): void;
  setHsl(hsl: string): void;
  setHsl() {
    if (typeof arguments[0] === 'string') {
      const match = arguments[0].match(HSLA);
      if (!match) throw new Error('Invalid hsl format: ' + arguments[0]);
      const h = parseInt(match[1] || '0');
      const s = parseInt(match[2] || '0');
      const l = parseInt(match[3] || '0');
      const a = parseFloat(match[4] || '1');
      this.setHsl(h, s, l, a);
    } else {
      this.hue = arguments[0];
      this.sat = arguments[1];
      this.lum = arguments[2];
      this.alp = arguments.length === 4 ? arguments[3] : 1;
    }
  }

  setHex(r: number, g: number, b: number, a: number): void;
  setHex(r: number, g: number, b: number): void;
  setHex(hex: string): void;
  setHex() {
    if (typeof arguments[0] === 'string') {
      let r: string, g: string, b: string, a: string;
      const hex = arguments[0].slice(1);
      switch (hex.length) {
        case 3:
        case 4:
          r = hex.at(0)!;
          g = hex.at(1)!;
          b = hex.at(2)!;
          a = hex.length === 4 ? hex.at(3)! : 'f';
          break;
        case 6:
        case 8:
          r = hex.slice(0, 2);
          g = hex.slice(2, 4);
          b = hex.slice(4, 6);
          a = hex.length === 8 ? hex.slice(6, 8) : 'ff';
          break;
        default:
          throw new Error('Invalid hex value: ' + arguments[0]);
      }
      const [h, s, l] = Color.rgbToHsl(
        Number('0x' + (r.length === 1 ? (r + r) : r)),
        Number('0x' + (g.length === 1 ? (g + g) : g)),
        Number('0x' + (b.length === 1 ? (b + b) : b)),
      );
      this.setHsl(h, s, l, Number('0x' + a) / (a.length === 1 ? 15 : 255));
    } else {
      const [h, s, l] = Color.rgbToHsl(
        arguments[0],
        arguments[1],
        arguments[2],
      );
      this.setHsl(h, s, l, arguments.length === 4 ? Number('0x' + arguments[3]) / 255 : 1);
    }
  }

  mixWith(color: Color, percent = 0.5) {
    if (percent < 0 || percent > 1) throw new Error('Percent must be between 0 and 1');

    const fa = percent;
    const af = 1 - fa;

    const avgSat = af * this.sat + color.sat * fa;
    const avgLum = af * this.lum + color.lum * fa;
    const avgAlp = af * this.alp + color.alp * fa;

    let deltaHue = color.hue - this.hue;
    if (deltaHue > 180) deltaHue -= 360;
    else if (deltaHue < -180) deltaHue += 360;

    let avgHue = (this.hue + deltaHue * fa) % 360;
    if (avgHue < 0) avgHue += 360;

    return new Color(avgHue, avgSat, avgLum, avgAlp);
  }

  saturate(percent = 0.5) {
    if (percent < 0 || percent > 1) throw new Error('Percent must be between 0 and 1');
    const diff = 100 - this.sat;
    const newSat = this.sat + diff * percent;
    return new Color(this.hue, newSat, this.lum, this.alp);
  }

  desaturate(percent = 0.5) {
    if (percent < 0 || percent > 1) throw new Error('Percent must be between 0 and 1');
    const newSat = this.sat - this.sat * percent;
    return new Color(this.hue, newSat, this.lum, this.alp);
  }

  lighten(percent = 0.5) {
    if (percent < 0 || percent > 1) throw new Error('Percent must be between 0 and 1');
    const diff = 100 - this.lum;
    const newLum = this.lum + diff * percent;
    return new Color(this.hue, this.sat, newLum, this.alp);
  }

  darken(percent = 0.5) {
    if (percent < 0 || percent > 1) throw new Error('Percent must be between 0 and 1');
    const newLum = this.lum - this.lum * percent;
    return new Color(this.hue, this.sat, newLum, this.alp);
  }

  withSaturation(sat: number) {
    if (sat < 0 || sat > 100) throw new Error('Saturation must be between 0 and 100');
    return new Color(this.hue, sat, this.lum, this.alp);
  }

  withLuminosity(lum: number) {
    if (lum < 0 || lum > 100) throw new Error('Luminance must be between 0 and 100');
    return new Color(this.hue, this.sat, lum, this.alp);
  }

  toString(): string {
    const [r, g, b] = Color.hslToRgb(this.hue, this.sat, this.lum);

    if (this.alp < 1) return `rgba(${r}, ${g}, ${b}, ${this.alp})`;

    const hex = (v: number) => {
      const h = v.toString(16);
      return h.length < 2 ? '0' + h : h;
    };

    return '#' + [r, g, b].map(hex).join('');
  }

  static rgbToHsl(r: number, g: number, b: number) {
    r /= 255;
    g /= 255;
    b /= 255;

    const cmin = Math.min(r, g, b);
    const cmax = Math.max(r, g, b);
    const delta = cmax - cmin;

    let h = 0, s = 0, l = 0;

    if (delta === 0) h = 0;
    else if (cmax === r) h = ((g - b) / delta) % 6;
    else if (cmax === g) h = (b - r) / delta + 2;
    else h = (r - g) / delta + 4;

    h = Math.round(h * 60);
    if (h < 0) h += 360;

    l = (cmax + cmin) / 2;
    s = delta === 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
    s = +(s * 100).toFixed(1);
    l = +(l * 100).toFixed(1);

    return [h, s, l];
  }

  toRgb() {
    const [r, g, b] = Color.hslToRgb(this.hue, this.sat, this.lum);
    return { r, g, b, a: this.alp };
  }

  static hslToRgb(h: number, s: number, l: number) {
    h = h % 360;
    s /= 100;
    l /= 100;

    const c = (1 - Math.abs(2 * l - 1)) * s;
    const x = c * (1 - Math.abs((h / 60) % 2 - 1));
    const m = l - c / 2;

    let r = 0, g = 0, b = 0;

    if (0 <= h && h < 60) {
      r = c;
      g = x;
      b = 0;
    } else if (60 <= h && h < 120) {
      r = x;
      g = c;
      b = 0;
    } else if (120 <= h && h < 180) {
      r = 0;
      g = c;
      b = x;
    } else if (180 <= h && h < 240) {
      r = 0;
      g = x;
      b = c;
    } else if (240 <= h && h < 300) {
      r = x;
      g = 0;
      b = c;
    } else if (300 <= h && h < 360) {
      r = c;
      g = 0;
      b = x;
    }

    r = Math.round((r + m) * 255);
    g = Math.round((g + m) * 255);
    b = Math.round((b + m) * 255);

    return [r, g, b];
  }
}
