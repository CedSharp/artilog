import Color from './color';
import Theme from './theme';
import ThemeBuilder from './theme-builder';

export default (isDark: boolean, texts: Record<string, Color>, palette: Record<string, Color>): Theme => {
  const ground = texts.text.withLuminosity(isDark ? 15 : 85);

  return new ThemeBuilder()
    .setPrefix('preview')
    .addText('text', texts.text)
    .addSurface('ground', { bg: ground, fg: texts.text })
    .get();
};
